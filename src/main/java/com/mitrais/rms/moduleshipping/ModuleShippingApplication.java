package com.mitrais.rms.moduleshipping;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient //supaya dia mendaftarkan diri ke Eureka

public class ModuleShippingApplication {

	public static void main(String[] args) {
		SpringApplication.run(ModuleShippingApplication.class, args);
	}

}
